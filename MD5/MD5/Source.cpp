#include <iostream>
#include <cstdio>
#include <string>
#include <vector>

using namespace std;

typedef unsigned int ui;

// MAX_INT * abs(sin(i))
ui K[64] = {0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
 0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 
 0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821, 
 0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 
 0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8, 
 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed, 
 0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, 
 0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c, 
 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, 
 0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05, 
 0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, 
 0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039, 
 0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1, 
 0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 
 0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 };

int s[64] = { 7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
 5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
 4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
 6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21 };

class BitArray {
public:
	BitArray() {
		base = 2;
	}

	BitArray(string stringArray) {
		base = 2;
		for (ui i = 0; i < stringArray.length(); i++) {
			int symbol = stringArray[i];
			vector<char> tmp;
			for (int j = 0; j < 8; j++) {
				int c = symbol % 2;
				symbol /= 2;
				tmp.push_back(c);
			}
			for (int j = 7; j >= 0; j--)
				elements.push_back(tmp[j]);
		}
		AppendBits();
	}

	//	���������� �����
	void AppendBits() {
		int curSize = Size();
		push_back(1);
		for (int i = 1; i < 448 - curSize; i++)
			push_back(0);
		vector<int> tmp;
		for (int i = 0; i < 8; i++) {
			tmp.push_back(curSize % 2);
			curSize /= 2;
		}
		for (int i = 7; i >= 0; i--)
			push_back(tmp[i]);
		for (int i = 0; i < 56; i++)
			push_back(0);
	}

	//	������� � 32-������� �������������
	void to32base() {
		base = 32;
		vector<int> tmpElements;
		for (int i = 0; i < 16; i++) {
			ui curElem = 0;
			ui curPower = 1 << 31;

			for (int j = 24; j < 32; j++) {
				curElem += curPower * (elements[i * 32 + j] == 0 ? 0 : 1);
				curPower >>= 1;
			}
			for (int j = 16; j < 24; j++) {
				curElem += curPower * (elements[i * 32 + j] == 0 ? 0 : 1);
				curPower >>= 1;
			}
			for (int j = 8; j < 16; j++) {
				curElem += curPower * (elements[i * 32 + j] == 0 ? 0 : 1);
				curPower >>= 1;
			}
			for (int j = 0; j < 8; j++) {
				curElem += curPower * (elements[i * 32 + j] == 0 ? 0 : 1);
				curPower >>= 1;
			}

			tmpElements.push_back(curElem);
		}
		elements.assign(tmpElements.begin(), tmpElements.end());
	}

	int Size() {
		return elements.size();
	}

	void Print() {
		if (base == 2)
			for (ui i = 0; i < elements.size();) {
				cout << "\t" << i / 8 << "\t";
				for (int j = 0; j < 8; j++, i++)
					cout << (j % 4 == 0 ? " " : "") << (int)elements[i];
				cout << endl;
			}	
		if (base == 32) {
			for (int i = 0; i < Size(); i++)
				cout << "\t" << i << "\t" << std::hex << elements[i] << endl;
		}
		
		cout << endl;
	}

	void push_back(char bit) {
		elements.push_back(bit);
	}

	ui& operator[] (int id) {
		return elements[id];
	}

private:
	int base;
	vector<ui> elements;
};

//	������������ ����
ui lowBytesFirst(ui a) {
	ui b1 = a % (1 << 8);
	ui b2 = a / (1 << 8) % (1 << 8);
	ui b3 = a / (1 << 16) % (1 << 8);
	ui b4 = a / (1 << 24);
	return b1 * (1 << 24) + b2 * (1 << 16) + b3 * (1 << 8) + b4;;
}

//	������� ����� �����
ui rotate_left(ui x, int n) {
	return (x << n) | (x >> (32 - n));
}

void Init() {
	freopen("output.txt", "w", stdout);
	setlocale(0, "rus");
}

int main() {
	Init();
	BitArray bits("Kib");
	printf("�������� ��������� � �������� �������\n");
	bits.Print();
	bits.to32base();
	printf("�������� ��������� � ����������������� �������\n");
	bits.Print();

	ui a0 = 0x67452301;
	ui b0 = 0xefcdab89;
	ui c0 = 0x98badcfe;
	ui d0 = 0x10325476;

	ui A = a0, B = b0, C = c0, D = d0;
	
	//	�������� MD5
	for (int i = 0; i < 64; i++) {
		if (!(i % 16)) {
			printf("%d �����\n", i / 16 + 1);
		}
		printf("\t%0x\t%0x\t%0x\t%0x\n", A, B, C, D);
		ui F;
		int g;
		if (i < 16) {
			F = (B & C) | (~B & D);
			g = i;
		}
		else if (i < 32) {
			F = (D & B) | (~D & C);
			g = (5 * i + 1) % 16;
		}
		else if (i < 48) {
			F = B ^ C ^ D;
			g = (3 * i + 5) % 16;
		}
		else {
			F = C ^ (B | (~D));
			g = (7 * i) % 16;
		}
		ui dTemp = D;
		D = C;
		C = B;
		B += rotate_left(A + F + K[i] + bits[g], s[i]);
		A = dTemp;
	}

	a0 += A;
	b0 += B;
	c0 += C;
	d0 += D;

	cout << endl << "�������� ��������� ����� �������������" << endl;
	cout << "\t" << std::hex << a0 << endl;
	cout << "\t" << std::hex << b0 << endl;
	cout << "\t" << std::hex << c0 << endl;
	cout << "\t" << std::hex << d0 << endl;

	cout << endl << "�������� ��������� ����� ������������" << endl;
	cout << "\t" << std::hex << lowBytesFirst(a0) << endl;
	cout << "\t" << std::hex << lowBytesFirst(b0) << endl;
	cout << "\t" << std::hex << lowBytesFirst(c0) << endl;
	cout << "\t" << std::hex << lowBytesFirst(d0) << endl;

	return 0;
}