#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <bitset>

using namespace std;

//	������������ ����� ��� �����
int PC1[56];

//	������ ������������ ����� �����
int PC2[48];

//	��������� ������������
int IP[64];

//	������� ���������� ���������
int E[48];

//	����������� ����� ������� � ������ ����� �����
int R[16] = { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

//	������� �������
int S[512];

//	������������ � ������� f
int P[32];

//	�������� ������������
int IPR[64];

//	���������� ����� ������� �� �������� ������
void readTable(int *arr, int size) {
	string tmp;
	cin >> tmp;
	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}
}

//	������������� ���������� ��� ������
void Init() {
	setlocale(0, "rus");
	freopen("init.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	readTable(PC1, 56);
	readTable(PC2, 48);
	readTable(IP, 64);
	readTable(E, 48);
	readTable(S, 8 * 4 * 16);
	readTable(P, 32);
	readTable(IPR, 64);
	return;
}

//	������������� ����� ��������� �����
void printBit(string bits, int row) {
	for (int i = 0; i < bits.size() / row; i++) {
		cout << "\t" << i + 1 << "\t";
		for (int j = 0; j < row; j++)
			cout << bits[i * row + j];
		cout << endl ;
	}
}

//	��������� �������� �� ������ 2
string operator ^(string s1, string s2) {
	if (s1.size() != s2.size()) {
		cout << "����� ����� �� ���������\n";
		exit(0);
	}

	string result = "";
	for (int i = 0; i < s1.size(); i++)
		result += (((s1[i] - '0') ^ (s2[i] - '0')) == 0) ? "0" : "1";
	return result;
}

//	����������� �����
void RotateLeft(string &s, int r) {
	string tmp = "";
	for (int i = r; i < s.size(); i++)
		tmp += s[i];
	for (int i = 0; i < r; i++)
		tmp += s[i];
	s = tmp;
}

//	������������ ���
string Permutate(string s, int* arr, int newSize) {
	vector<char> charKey(newSize);
	for (int i = 0; i < newSize; i++) {
		charKey[i] = s[arr[i] - 1];
	}
	return string(charKey.begin(), charKey.begin() + newSize);
}

//	������� 6-��� -> 4-���
string Narrow(string s) {
	string answer = "";

	for (int i = 0; i < 8; i++) {
		int cr = i * 6;

		//	�� ������� � ���������� ���� �������� ����� ������
		char number[2];
		number[0] = s[cr];
		number[1] = s[cr + 5];
		string tmp(number, number + 2);
		int n = stoi(tmp, 0, 2);

		//	�� 4 ������� ����� �������� ����� �������
		char row[4];
		row[0] = s[cr + 1];
		row[1] = s[cr + 2];
		row[2] = s[cr + 3];
		row[3] = s[cr + 4];
		tmp = string(row, row + 4);
		int r = stoi(tmp, 0, 2);

		int Narrowed = S[i * 64 + n * 16 + r];

		for (int d = 0; d < 4; d++) {
			int bit = Narrowed % 2;
			Narrowed /= 2;
			if (bit)
				tmp[d] = '1';
			else
				tmp[d] = '0';
		}
		answer += string(tmp.rbegin(), tmp.rend());
	}
	return answer;
}

//	������� ����������
string f(string r, string key) {
	//	���������� ��������� �� 48 ���
	string er = Permutate(r, E, 48);
	//	�������� �� ������ � ������
	string erXorKey = er ^ key;
	//	������� �����
	string narrow = Narrow(erXorKey);
	return Permutate(narrow, P, 32);
}

void DES_CBC() {
	string message = "��������";
	string messageBit = "";
	for (int i = 0; i < message.size(); i++) {
		bitset<8> b(message[i]);
		messageBit += b.to_string();
	}
	printf("�������� DES_CBC:\n");
	printf("\t��������� ���������\n\t���������� ������������� = %s\n", message.c_str());
	printf("\n\t������� �������������\n");
	printBit(messageBit, 8);

	string sp;
	for (int i = 0; i < 32; i++) {
		sp += "10";
	}
	printf("\n\t�������������\n");
	printBit(sp, 8);

	cout << "\n\t��������� ���������� ��������\n";
	printBit(sp ^ messageBit, 8);

	string sKey = Permutate(messageBit, PC1, 56);
	messageBit = sp ^ messageBit;

	printf("\n\t����\n");
	printBit(sKey, 7);
	
	vector<string> keys(16);
	string curKey(sKey.begin(), sKey.end());

	//	��������� �������� ���������
	for (int i = 0; i < 16; i++) {
		string c(curKey.begin(), curKey.begin() + 28);
		string d(curKey.begin() + 28, curKey.begin() + 56);

		RotateLeft(c, R[i]);
		RotateLeft(d, R[i]);

		curKey = c + d;

		char charKey[48];
		for (int i = 0; i < 48; i++) {
			charKey[i] = curKey[PC2[i] - 1];
		}

		keys[i] = Permutate(curKey, PC2, 48);

		printf("\n\t���� �%d\n", i + 1);
		printBit(keys[i], 6);
	}

	string IPmessage = Permutate(messageBit, IP, 64);
	printf("\n\t��������� ����� ������������\n");
	printBit(IPmessage, 8);

	string prevL(IPmessage.begin(), IPmessage.begin() + 32);
	string prevR(IPmessage.begin() + 32, IPmessage.end());

	for (int i = 0; i < 16; i++) {
		printf("\n\t%d �����", i + 1);
		printf("\n\tLi\n");
		printBit(prevL, 8);

		printf("\n\tRi\n");
		printBit(prevR, 8);

		printf("\n\tf\n");
		printBit(f(prevR, keys[i]), 8);

		string curL = prevR;
		string curR = prevL ^ f(prevR, keys[i]);

		printf("\n\tHi ^ f(ki, Li)\n");
		printBit(curR, 8);

		prevR = curR;
		prevL = curL;
	}

	string preAnswer = prevR + prevL;
	string answer = Permutate(preAnswer, IPR, 64);
	printf("\n\t�������� ������������ RPC\n");
	printBit(answer, 8);
}

int main() {

	Init();

	DES_CBC();

	return 0;
}