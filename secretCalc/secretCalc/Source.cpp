#include <bitset>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>

using namespace std;

void Init() {
	setlocale(0, "rus");
	freopen("output.txt", "w", stdout);
}

//	���������� ����� ��������
int gcd(int a, int b) {
	return b ? gcd(b, a % b) : a;
}

//	������� ���������� � �������
unsigned int fastPow(unsigned int a, int k, int modulo) {
	unsigned int res = 1;
	while (k) {
		if (k & 1) {
			res *= a;
			res %= modulo;
			k--;
		}
		else {
			a *= a;
			a %= modulo;
			k >>= 1;
		}
	}
	return res;
}

//	�������� ����� �� ������
int gcdExt(int a, int b) {
	if (a > b)
		a %= b;
	int x1 = 1, y1 = 0, c1 = a;
	int x2 = 0, y2 = 1, c2 = b;
	while (a * x1 + b * y1 != 0) {
		int k = c2 / c1;
		x2 -= k * x1;
		y2 -= k * y1;
		c2 -= k * c1;
		swap(x1, x2);
		swap(y1, y2);
		swap(c1, c2);
	}
	if (x2 < 0)
		x2 = b + x2;
	return x2;
}

void Average() {
	//	��������� ������ 3 ���� ������� � ��������
	int Numbers[3] = { 12, 10, 2 };
	printf("������ �������������� ���������� ��� ���������� ��������\n");
	printf("\t�������� �����:\n\t");
	for (int i = 0; i < 3; i++)
		printf("%d ", Numbers[i]);

	int OpenKey[3] = { 5, 7, 17 };
	int n = 91;
	printf("\n�������� �������� ������ ��� ������� ���������\n");
	for (int i = 0; i < 3; i++) {
		printf("\t{%d, %d}\n", OpenKey[i], n);
	}

	int PrivateKey[3] = { 29, 31, 17 };
	printf("\n�������� �������� ������ ��� ������� ���������\n");
	for (int i = 0; i < 3; i++) {
		printf("\t{%d}\n", PrivateKey[i]);
	}

	printf("\n���������� �������� ��������\n");

	int x = 3;
	printf("\tx = %d\n", x);

	int C1 = fastPow(Numbers[0] + x, OpenKey[1], n);
	printf("\t�����, ����������� ������ = %d\n", C1);

	int dC1 = fastPow(C1, PrivateKey[1], n);
	printf("\t�����, �������������� ������ = %d\n", dC1);
	int C2 = fastPow(dC1 + Numbers[1], OpenKey[2], n);
	printf("\t�����, ����������� ������ = %d\n", C2);

	int dC2 = fastPow(C2, PrivateKey[2], n);
	printf("\t�����, �������������� ������� = %d\n", dC2);
	int C3 = fastPow(dC2 + Numbers[2], OpenKey[0], n);
	printf("\t�����, ����������� ������� = %d\n", C3);

	int dC3 = fastPow(C3, PrivateKey[0], n);
	dC3 -= x;
	printf("\t�����, �������������� ������ = %d\n", dC3);
	double result = dC3 * 1. / 3;
	printf("\t������� �������� = %f\n", result);
}

void Splitting() {	
	printf("\n��������� ������� � �������������� ������������\n");

	string secret = "���";
	printf("\t��������� ����� = %s\n", secret.c_str());

	int digitSecret[3] = { secret[0], secret[1], secret[2] };
	printf("\t������� ������������� ���� �����\n");
	for (int i = 0; i < 3; i++) {
		bitset<8> t(digitSecret[i]);
		cout << "\t" << secret[i] << " " << t << endl;
	}

	printf("\n��������� ��������� ���� � �������� �� ����������\n");
	int g1[3] = { '�', '�', '�' };
	int g2[3] = { '�', '�', '�' };
	int g3[3] = { '�', '�', '�' };

	printf("�����1\n");
	for (int i = 0; i < 3; i++) {
		printf("\t%c - ", g1[i]);
		bitset<8> t(g1[i]);
		cout << t << endl;
	}

	printf("\n�����2\n");
	for (int i = 0; i < 3; i++) {
		printf("\t%c - ", g2[i]);
		bitset<8> t(g2[i]);
		cout << t << endl;
	}

	printf("\n�����3\n");
	for (int i = 0; i < 3; i++) {
		printf("\t%c - ", g3[i]);
		bitset<8> t(g3[i]);
		cout << t << endl;
	}

	printf("\n�����������\n");
	int cipher[3];
	for (int i = 0; i < 3; i++) {
		cipher[i] = digitSecret[i] ^ g1[i] ^ g2[i] ^ g3[i];
		bitset<8> t(cipher[i]);
		cout << "\t" << t << endl;
	}
	
	printf("\n�������������� �������\n");
	int newSecret[3];
	for (int i = 0; i < 3; i++) {
		newSecret[i] = cipher[i] ^ g1[i] ^ g2[i] ^ g3[i];
		bitset<8> t(newSecret[i]);
		cout << "\t" << t;
		printf(" = %c\n", newSecret[i]);
	}

}

class Fraction {
public:
	int a, b;
	Fraction() {
		a = 0;
		b = 1;
	}

	Fraction(int a, int b) : a(a), b(b) {}

	void Simplify() {
		int GCD = gcd(a, b);
		a /= GCD;
		b /= GCD;
	}

	Fraction operator +(Fraction l) {
		Fraction r = *this;
		Fraction res;
		
		res.a = l.a * r.b + l.b * r.a;
		res.b = r.b * l.b;
		res.Simplify();

		return res;
	}

	Fraction operator *(int l) {
		Fraction r = *this;
		Fraction res;
		res.a = r.a * l;
		res.b = r.b;

		res.Simplify();

		return res;
	}

	void Print() {
		printf("%d/%d\n", a, b);
	}
};

void Shamir() {
	printf("\n���������� ������� �� ����� ������\n");

	int S = 12;
	printf("\t������ = %d\n", S);

	int p = 59;
	printf("\tp = %d\n", p);

	int a2 = 10, a1 = 23;
	printf("\ta2 = %d, a1 = %d\n", a2, a1);
	printf("\tf(x) = (%dx * x + %dx + %d)\n", a2, a1, S);

	printf("\n����������� �����\n");
	int x[5], y[5];
	for (int i = 0; i < 5; i++) {
		x[i] = i + 1;
		y[i] = (x[i] * x[i] * a2 + x[i] * a1 + S) % p;
		printf("\ty%d = %d\n", i, y[i]);
	}

	printf("\n�������������� ������\n");
	printf("\n����� ����� ��� �������������� ������\n");
	printf("\t(%d, %d)\n", x[1], y[1]);
	printf("\t(%d, %d)\n", x[2], y[2]);
	printf("\t(%d, %d)\n", x[4], y[4]);

	int l1[3] = { 1, -8, 15 };
	int l2[3] = { 1, -7, 10 };
	int l4[3] = { 1, -5, 6 };

	vector<Fraction> k(3);
	k[0] = Fraction(1, 3) * y[1];
	k[1] = Fraction(-1, 2) * y[2];
	k[2] = Fraction(1, 6) * y[4];

	vector<Fraction> coef(3);
	for (int i = 0; i < 3; i++) {
		coef[i] = k[0] * l1[i] + k[1] * l2[i] + k[2] * l4[i];
	}

	printf("\nL(x):\n");
	printf("\tx * x * ");
	coef[0].Print();

	printf("\tx * ");
	coef[1].Print();

	printf("\t");
	coef[2].Print();

	printf("\tmod %d\n", p);

	int K[3];
	for (int i = 0; i < 3; i++) {
		K[i] = coef[i].a * gcdExt(coef[i].b, p);
		K[i] %= p;
		if (K[i] < 0)
			K[i] += p;
	}
	printf("\nL(x):\n");
	printf("\tx * x * %d + ", K[0]);

	printf("x * %d + ", K[1]);

	printf("%d mod %d", K[2], p);
}

void printArray(int *arr, int size, char arrName) {
	printf("\t%c = {", arrName);
	for (int i = 0; i < size; i++) {
		if (i == size - 1) {
			printf("%d}\n", arr[i]);
			break;
		}
		printf("%d ", arr[i]);
	}
}

void printArray(vector<pair<int, int> > arr, char arrName) {
	printf("\t%c:\n", arrName);
	for (int i = 0; i < arr.size(); i++)
		printf("\t\t{%d, %d}\n", arr[i].first, arr[i].second);
}

void AsmuthBloom() {
	printf("\n���������� ������� �� ����� ������-�����\n");

	int S = 12;
	printf("\tS = %d\n", S);

	int p = 13;
	printf("\tp = %d\n", p);

	int d[5] = { 17, 20, 23, 29, 37 };
	printArray(d, 5, 'd');

	int r = 30;
	printf("\tr = %d\n", r);

	int S_ = S + r * p;
	printf("\tS' = %d\n", S_);

	int k[5];
	for (int i = 0; i < 5; i++) {
		k[i] = S_ % d[i];
	}
	printArray(k, 5, 'k');

	//	��������� ����
	printf("\n��������� �������������� �������\n");
	int ss[3] = { 2, 3, 5 };
	vector<pair<int, int> > shares(3);
	int D = 1;
	for (int i = 0; i < 3; i++) {
		int nr = ss[i] - 1;
		shares[i] = make_pair(d[nr], k[nr]);
		D *= d[nr];
	}

	printArray(shares, 's');
	printf("\tD = %d\n", D);

	int Dj[3];
	for (int i = 0; i < 3; i++) {
		Dj[i] = D / shares[i].first;
	}
	printArray(Dj, 3, 'D');

	int Dj_[3];
	for (int i = 0; i < 3; i++) {
		Dj_[i] = gcdExt(Dj[i], shares[i].first);
	}
	printArray(Dj_, 3, 'R');

	S_ = 0;
	for (int i = 0; i < 3; i++) {
		S_ += shares[i].second * Dj[i] * Dj_[i];
		S_ %= D;
	}
	printf("\tS' = %d\n", S_);

	int getS = S_ % p;
	printf("\tS = %d\n", getS);
}

int main() {
	Init();

	Average();
	Splitting();
	Shamir();
	AsmuthBloom();

	return 0;
}