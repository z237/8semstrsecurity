#include <iostream>
#include <cstdio>
#include <string>
#include <bitset>
#include <vector>

using namespace std;

string DigitAlgo = "�������������������";
vector<int> digits;
int N[60];

void evenOdd() {
	string s = "Kibel";
	printf("�������� ���������\n\t�����\t����\t���������� ���\n\t\t\t\t  �����\t���\n");
	for (unsigned int i = 0; i < s.size(); i++) {
		bitset<8> a((int)s[i]);
		printf("\t%c\t", s[i]);
		cout << a;
		int even = 0;
		for (int i = 0; i < a.size(); i++) {
			if (a[i] == 1)
				even++;
		}
		printf("  %d\t\t%d\n", (even + 1) % 2, even % 2);
	}
}

void LunasAlgorithm() {
	int evenSum = 0;
	int oddSum = -digits[14];
	for (int i = 0; i < 15; i++) {
		if (i % 2 == 1)
			evenSum += digits[i] * 2 % 9;
		else
			oddSum += digits[i];
	}
	int cd = 10 - (evenSum + oddSum) % 10;
	printf("\n�������� ����\n\t����� �� ������ = %d\n\t����� �� �������� = %d\n\tCD = %d\n", evenSum, oddSum, cd);
}

void EAN_13() {
	int evenSum = 0;
	int oddSum = -digits[12];
	for (int i = 0; i < 13; i++) {
		if (i % 2 == 1) {
			evenSum += 3 * digits[i];
		}
		else {
			oddSum += digits[i];
		}
	}
	int cd = 10 - (evenSum + oddSum) % 10;
	printf("\n�������� EAN-13\n\t����� ������ = %d\n\t����� �������� = %d\n\tCD = %d\n", evenSum, oddSum, cd);
}

//	���
void INT() {
	int mults[10] = { 2, 4, 10, 3, 5, 9, 4, 6, 8 };
	int n10 = 0;
	for (int i = 0; i < 10; i++) {
		n10 += mults[i] * digits[i];
	}
	n10 %= 11;
	n10 %= 10;
	printf("\n��� ����������� ����\n\tn10 = %d\n", n10);
}

void JD() {
	int n6 = 0;
	for (int i = 0; i < 5; i++) {
		n6 += (i + 1) * digits[i];
	}
	n6 %= 11;
	printf("\n���� ������� �� �� ����������\tn6 = %d\n", n6);
}

void CRC(char c) {
	int x = c - '�' + 1;
	bitset<4> letter(x);
	int n = 16;
	int g = 19;
	int answer = x * n;
	answer %= g;
	bitset<4> a(answer);
	printf("\tCRC(%c) = ", c);
	cout << a << endl;
}

void CRCcall() {
	printf("\n����������� �����\n");
	CRC('�');
	CRC('�');
	CRC('�');
}

void readTable(int *arr, int size) {
	string tmp;
	cin >> tmp;
	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}
}

void matrixMulti(string a, int *b, vector<int> &answer) {
	for (int j = 0; j < 4; j++)
		for (int i = 0; i < 15; i++) {
			answer[j] += N[j * 15 + i] * (a[i] == '0' ? 0 : 1);
			answer[j] %= 2;
		}
}

void printVector(vector<int> a, char vectorName) {
	printf("\t%c = {", vectorName);
	for (int i = 0; i < 4; i++) {
		printf("%d", a[i]);
		if (i != 3)
			cout << ", ";
	}
	printf("}\n");
}

int parityBit(string s) {
	int pb = 0;
	for (int i = 0; i < s.size(); i++)
		pb += (s[i] == '0' ? 0 : 1);
	return (pb + 1) % 2;
}

void ECC() {
	printf("\n��� ��������� ������\n");
	int c1 = 'k';
	int c2 = 'i';
	int data = c1 * (1 << 8) + c2;
	char sData[16];
	for (int i = 0; i < 16; i++) {
		sData[15 - i] = (data % 2 == 0) ? '0' : '1';
		data /= 2;
	}

	string ss(sData, sData + 11);
	printf("\t��������� ������ = ");
	cout << ss << endl;

	string xr = "";
	for (int i = 0, k = 0; i < 11; i++) {
		while (k == 0 || k == 1 || k == 3 || k == 7) {
			xr += '0';
			k++;
		}
		k++;
		xr += ss[i];
	}
	printf("\tXR = ");
	cout << xr << endl;

	vector<int> r(4, 0);
	matrixMulti(xr, N, r);
	printVector(r, 'r');

	string recievedData(xr.begin(), xr.end());
	recievedData[0] = (r[0] == 0 ? '0' : '1');
	recievedData[1] = (r[1] == 0 ? '0' : '1');
	recievedData[3] = (r[2] == 0 ? '0' : '1');
	recievedData[7] = (r[3] == 0 ? '0' : '1');

	vector<int> S(4, 0);
	printf("\n\t��������� �������� ��� ������: %s\n", recievedData.c_str());
	matrixMulti(recievedData, N, S);
	printVector(S, 'S');
	printf("\t���������� ��� = %d\n", parityBit(recievedData));

	recievedData[10] = (recievedData[10] == '1' ? '0' : '1');
	printf("\n\t��������� �������� � ����� �������: %s\n", recievedData.c_str());
	matrixMulti(recievedData, N, S);
	printVector(S, 'S');
	printf("\t���������� ��� = %d\n", parityBit(recievedData));

	recievedData[12] = (recievedData[12] == '1' ? '0' : '1');
	printf("\n\t��������� �������� � ����� ��������: %s\n", recievedData.c_str());
	matrixMulti(recievedData, N, S);
	printVector(S, 'S');
	printf("\t���������� ��� = %d\n", parityBit(recievedData));
}

void Init() {
	setlocale(0, "rus");
	freopen("output.txt", "w", stdout);
	freopen("init.txt", "r", stdin);
	readTable(N, 60);
	for (int i = 0; i < DigitAlgo.size(); i++) {
		int x = DigitAlgo[i] - '�' + 1;
		if (x > 9) {
			digits.push_back(x / 10);
			digits.push_back(x % 10);
		}
		else {
			digits.push_back(x);
		}
	}
}

int main() {
	Init();

	evenOdd();
	LunasAlgorithm();
	EAN_13();
	INT();
	JD();
	CRCcall();
	ECC();

	return 0;
}