#include <iostream>
#include <cstdio>
#include <vector>
#include <ctime>

using namespace std;

vector<int> primes;
//	������ ���������� ��� ������ ������� ����� ������� N
void eratosthenesSieve() {
	int sieveSize = 300;
	vector<bool> prime(sieveSize, true);
	prime[0] = prime[1] = false;
	for (int i = 2; i < sieveSize; i++)
		if (prime[i])
			if (i * i < sieveSize)
				for (int j = i * i; j < sieveSize; j += i)
					prime[j] = false;
	for (int i = 0; i < sieveSize; i++)
		if (prime[i])
			primes.push_back(i);
}

void Init() {
	eratosthenesSieve();
	setlocale(0, "rus");
	freopen("output.txt", "w", stdout);
	//srand(time(NULL));
}

//	���������� ����� ��������
int gcd(int a, int b) {
	return b ? gcd(b, a % b) : a;
}

//	�������� ����� �� ������
int gcdExt(int a, int b) {
	if (a > b)
		a %= b;
	int x1 = 1, y1 = 0, c1 = a;
	int x2 = 0, y2 = 1, c2 = b;
	while (a * x1 + b * y1 != 0) {
		int k = c2 / c1;
		x2 -= k * x1;
		y2 -= k * y1;
		c2 -= k * c1;
		swap(x1, x2);
		swap(y1, y2);
		swap(c1, c2);
	}
	if (x2 < 0)
		x2 = b + x2;
	return x2;
}

//	������� ���������� � �������
unsigned int fastPow(unsigned int a, int k, int modulo) {
	unsigned int res = 1;
	while (k) {
		if (k & 1) {
			res *= a;
			res %= modulo;
			k--;
		}
		else {
			a *= a;
			a %= modulo;
			k >>= 1;
		}
	}
	return res;
}

void RSA() {
	printf("RSA\n��������� ������:\n");
	int id1 = rand() % primes.size();
	int id2 = id1;
	while (id1 == id2)
		id2 = rand() % primes.size();
	int p = primes[id1];
	int q = primes[id2];
	printf("\tp = %d and q = %d\n", p, q);

	int n = p * q;
	printf("\tn = p * q = %d\n", n);

	int euler = (p - 1) * (q - 1);
	printf("\tphi(n) = (p - 1) * (q - 1) = %d\n", euler);

	int e = euler;
	while (gcd(e, euler) != 1)
		e = rand() % euler;
	printf("\te = %d\n", e);

	int d = gcdExt(e, euler);
	printf("\td = %d\n", d);

	printf("\n��������������:\n");

	unsigned int k = 401;
	printf("\tk = %lu\n", k);

	unsigned int r = fastPow(k, e, n);
	printf("\tr = %lu\n", r);

	unsigned int kTest = fastPow(r, d, n);
	printf("\tk' = %lu\n\n", kTest);
}

void Schnorr() {
	printf("����� ������\n��������� ������:\n");
	int id1 = rand() % primes.size();
	if (id1 < primes.size() * 3 / 4)
		id1 += primes.size() / 4;
	int p = primes[id1];
	printf("\tp = %d\n", p);
	
	vector<int> dividers;
	for (int i = 1; i < primes.size(); i++) {
		if (primes[i] == p)
			break; 
		if ((p - 1) % primes[i] == 0)
			dividers.push_back(primes[i]);
	}

	int id2 = rand() % dividers.size();
	int q = dividers[id2];
	printf("\tq = %d\n", q);

	int x = rand() % q;
	printf("\tx = %d\n", x);

	unsigned int g = -1;
	for (int i = 2; i < p; i++) {
		unsigned int ost = fastPow(i, q, p);
		if (ost == 1) {
			g = i;
			break;
		}
	}
	printf("\tg = %lu\n", g);

	unsigned int tmp = fastPow(g, x, p);
	unsigned int y = gcdExt(tmp, p);
	printf("\ty = %lu\n", y);

	printf("\n��������������:\n");
	int k = rand() % q;
	k = 3;
	printf("\tk = %d\n", k);

	unsigned int r = fastPow(g, k, p);
	printf("\tr = %lu\n", r);

	int e = rand() % 128;
	printf("\te = %d\n", e);

	unsigned int s = (k + x * e) % q;
	printf("\ts = %lu\n", s);

	unsigned int tryR = fastPow(g, s, p) * fastPow(y, e, p) % p;
	printf("\tr = g^s * y^e mod p = %lu\n\n", tryR);
}

//	������� V
int calcV(int n) {
	for (int i = 2; i <= n; i++) {
		int v = i * i % n;
		int v_ = gcdExt(v, n);
		if (v * v_ % n == 1)
			return v;
	}
}

int calcS(int v_, int n) {
	int s = 1;
	while (s * s % n != v_)
		s++;
	return s;
}

void FFS() {
	printf("����� �����-�����-������\n��������� ������:\n");

	int id1 = rand() % primes.size();
	int id2 = id1;
	while (id1 == id2)
		id2 = rand() % primes.size();
	int q = primes[id1];
	int p = primes[id2];
	printf("\tp = %d, q = %d\n", p, q);

	int n = p * q;
	printf("\tn = p * q = %d\n", n);

	int v = calcV(n);
	int v_ = gcdExt(v, n);
	printf("\tv = %d, v' = %d\n", v, v_);

	int s = calcS(v_, n);
	printf("\ts = %d\n", s);

	printf("\n��������������:\n");


	int r = rand() % n;
	printf("\tr = %d\n", r);

	int z = r * r % p;
	printf("\tz = %d\n", z);

	int yb0 = r;
	int yb1 = (r * s) % p;
	printf("\tb = 0 => y = %d, b = 1 => y = %d\n", yb0, yb1);

	int zb0 = r * r % p;
	int zb1 = yb1 * yb1 * v % p;
	printf("\tb = 0 => z = %d, b = 1 => z = %d\n", zb0, zb1);
}

int main() {
	Init();

	RSA();
	Schnorr();
	FFS();
	
	return 0;
}