#include <iostream>
#include <cstdio>
#include <vector>
#include <string>

using namespace std;

//	��������� ��� ������������� ������
int n = 41;
int a = 3;
int b = 7;

//	������� ���������� � �������
unsigned fastPow(unsigned int a, int k, int modulo) {
	unsigned int res = 1;
	while (k) {
		if (k & 1) {
			res *= a;
			res %= modulo;
			k--;
		}
		else {
			a *= a;
			a %= modulo;
			k >>= 1;
		}
	}
	return res;
}

void RSA() {
	int h = '�' - '�';
	int d = 29;
	int n = 91;
	int e = 5;
	unsigned s = fastPow(h, d, n);
	printf("RSA\n\th = %d\n", h);
	printf("\t�������� ������� S = %lu\n", s);
	unsigned h_ = fastPow(s, e, n);
	printf("\th'' = %lu\n", h_);
}

void roct34_10_94() {
	printf("\n���� 34.10-94\n��������� ������\n");
	int p = 29;
	int q = 7;
	int a;
	for (int i = 2; i < p; i++)
		if ((int) pow(i, q) % p == 1) {
			a = i;
			break;
		}
	printf("\tp = %d, q = %d\n\ta = %d\n", p, q, a);

	int x = 3;
	int y = fastPow(a, x, p);
	printf("\tx = %d\n\ty = %d\n", x, y);
	
	printf("\n���������� �������� �������\n");
	int h = '�' - '�';
	int	k = 5;
	int w = fastPow(a, k, p);	
	int w_ = w % q;				
	int s = (x * w_ + k * h) % q;
	printf("\th = %d\n\tk = %d\n\tw = %d\n\tw_ = %d\n\ts = %d\n", h, k, w, w_, s);

	printf("\n�������� ���������\n");
	int v = fastPow(h, q - 2, q);
	int z1 = (s * v) % q;		
	int z2 = ((q - w_) * v) % q;
	int u = fastPow(a, z1, p) * fastPow(y, z2, p) % p % q;
	printf("\th' = %d\n\tv = %d\n\tz1 = %d\n\tz2 = %d\n\tu = %d\n", h, v, z1, z2, u);
	string ok = "\n�������� ��������: ";
	if (w_ == u)
		ok += "��";
	else
		ok += "���";
	cout << ok << endl;
}

//	�������� ����� �� ������
int gcdExt(int a, int b) {
	if (a > b)
		a %= b;
	int x1 = 1, y1 = 0, c1 = a;
	int x2 = 0, y2 = 1, c2 = b;
	while (a * x1 + b * y1 != 0) {
		int k = c2 / c1;
		x2 -= k * x1;
		y2 -= k * y1;
		c2 -= k * c1;
		swap(x1, x2);
		swap(y1, y2);
		swap(c1, c2);
	}
	if (x2 < 0)
		x2 = b + x2;
	return x2;
}

struct Point {
	int x, y;

	Point(Point *p) : x(p->x), y(p->y) {}

	Point(int x, int y) : x(x), y(y) {};

	Point() : x(0), y(0) {}

	bool operator == (Point p2) {
		return x == p2.x && y == p2.y;
	}

	void Print(char message[]) {
		printf("\t%s{%d; %d}\n", message, x, y);
	}

	Point operator +(Point p2) {
		Point p1(this);
		Point p3;
		int lambda;

		if (p1 == p2) {
			int denominator = gcdExt(2 * p1.y, n);
			lambda = (3 * p1.x * p1.x + a) * denominator % n;
		}
		else {
			int dx = p2.x - p1.x;
			int dy = p2.y - p1.y;

			if (dx < 0)
				dx += n;
			if (dy < 0)
				dy += n;

			int denominator = gcdExt(dx, n);
			lambda = dy * denominator % n;
		}

		if (lambda < 0)
			lambda += n;

		p3.x = lambda * lambda - p1.x - p2.x;
		p3.x %= n;
		if (p3.x < 0)
			p3.x += n;
		
		p3.y = lambda * (p1.x - p3.x) - p1.y;
		p3.y %= n;
		if (p3.y < 0)
			p3.y += n;

		return p3;
	}

	Point operator * (int d) {
		Point answer(this);
		Point q(this);
		d--;
		while (d) {
			if (d & 1) {
				answer = answer + q;
				d--;
			}
			q = q + q;
			//////////q.Print();
			d /= 2;
		}

		return answer;
	}
};

void roct34_10_2001() {
	printf("\n���� 34.10-2012\n��������� ������\n");
	int n = 41;
	int A = 3;
	int B = 7;
	printf("\tn = %d\n\tA = %d, B = %d\n", n, A, B);

	Point P(7, 17);
	P.Print("P(xp, yp) = ");

	int q = 47;
	int d = 10;
	printf("\tq = %d\n\td = %d\n", q, d);

	Point Q = P * 10;
	Q.Print("Q(xq, yq) = ");

	printf("\n���������� �������� �������\n");
	int h = '�' - '�' + 1;	
	int e = h % q;			
	int k = 11;				
	printf("\th = %d\n\te = %d\n\tk = %d\n", h, e, k);

	Point C = P * k;
	C.Print("C(xc, yc) = ");

	int r = C.x % q;			
	printf("\tr = %d\n", r);

	int s = (r * d + k * e) % q;
	printf("\ts = %d\n", s);
	
	printf("\n�������� ����������� ���������\n");
	int v = gcdExt(e, q);
	printf("\tv = %d\n", v);

	int z1 = (s * v) % q;
	int z2 = ((q - r) * v) % q;	
	printf("\tz1 = %d\n\tz2 = %d\n", z1, z2);

	//////////(P * z1).Print("");
	//////////(Q * z2).Print("");
	Point C_ = P * z1 + Q * z2;
	C_.Print("C'(x'c, y'c) = ");
	string ok = "\n�������� ��������: ";
	if (C == C_)
		ok += "��";
	else
		ok += "���";
	cout << ok << endl;
}

void Init() {
	setlocale(0, "rus");
	freopen("output.txt", "w", stdout);
}

int main() {
	Init();

	RSA();
	roct34_10_94();
	roct34_10_2001();

	return 0;
}